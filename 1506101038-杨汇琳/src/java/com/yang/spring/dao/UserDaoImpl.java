package com.yang.spring.dao;

/**
 * Created by dell on 2017/9/21.
 */
public class UserDaoImpl implements IUserDao {

    @Override
    public void addId(int id) {
        System.out.println("addId is " + id);
    }

    @Override
    public void addUsername(String username) {
        System.out.println("addUsername is " + username);
    }
}
