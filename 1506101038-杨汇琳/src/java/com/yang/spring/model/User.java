package com.yang.spring.model;

/**
 * Created by dell on 2017/9/21.
 */
public class User {
    public int id;
    public String userName;

    public User(int id, String userName) {
        this.id = id;
        this.userName = userName;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                '}';
    }

}
