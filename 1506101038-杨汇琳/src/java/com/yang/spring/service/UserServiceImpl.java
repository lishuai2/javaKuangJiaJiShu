package com.yang.spring.service;

import com.yang.spring.dao.UserDaoImpl;

/**
 * Created by dell on 2017/9/21.
 */
public class UserServiceImpl implements IUserService {

    private UserDaoImpl userDao;


    @Override
    public void addId(int id) {
getUserDao().addId(id);
    }

    @Override
    public void addUsername(String username) {
getUserDao().addUsername(username);
    }


    public void setUserDao(UserDaoImpl userDao) {
        this.userDao = userDao;
    }

    public UserDaoImpl getUserDao() {
        return userDao;
    }
}
