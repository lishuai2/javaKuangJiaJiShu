package com.yang.spring.controller;

import com.yang.spring.service.UserServiceImpl;

/**
 * Created by dell on 2017/9/21.
 */
public class UserController {
    UserServiceImpl userService;


    public UserServiceImpl getUserService() {
        return userService;
    }

    public void setUserService(UserServiceImpl userService) {
        this.userService = userService;
    }

    public void addId(int id) {
        getUserService().addId(id);
    }

    public void addUsername(String username) {
        getUserService().addUsername(username);
    }


//    public UserController(UserServiceImpl userService) {
//        this.userService = userService;
//    }
}
