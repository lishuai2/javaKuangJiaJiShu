import com.yang.spring.controller.UserController;
import com.yang.spring.model.HelloWorld;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by dell on 2017/9/21.
 */
public class test01 {
        @org.junit.Test
    public void hello() {
        ApplicationContext fct = new ClassPathXmlApplicationContext("beans.xml");
        HelloWorld helloWorld = (HelloWorld) fct.getBean("helloWorld");
        helloWorld.hello();
    }


    //通过工厂获取spring对象
    BeanFactory factory = new ClassPathXmlApplicationContext("beans.xml");
//    @Test
//    public void testHello() {
//        HelloWorld helloWorld = (HelloWorld) factory.getBean("helloWorld",HelloWorld.class);
//        helloWorld.hello();
//    }


    @Test
    public void testUserController() {

        UserController userController = (UserController) factory.getBean("userController", UserController.class);
        userController.addId(1506101038);
        userController.addUsername("杨汇琳");

    }


}
