import controller.UserController;
import model.HelloWorld;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by home on 2017/9/21.
 */
public class TestClass {
    ApplicationContext fct = new ClassPathXmlApplicationContext("bean.xml");
    @org.junit.Test
    public void hello() {

        HelloWorld helloWorld = (HelloWorld) fct.getBean("helloWorld");
        helloWorld.hello();
    }
    @Test
  public void  testUserController(){
      UserController userController=(UserController) fct.getBean("userController");
        userController.addId(12);
        userController.addUserName("黎子新");
    }
}
