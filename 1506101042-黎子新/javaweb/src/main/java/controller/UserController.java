package controller;

import service.UserServiceImpl;

import javax.annotation.Resource;

/**
 * Created by home on 2017/9/21.
 */
public class UserController {
//    同在service里面申明dao层的一个属性一样，在Controller层里面就需要申明service层的属性，
// 并添加set和get方法，这就是一种依赖注入，依赖注入service的东西。

    UserServiceImpl userService;

    public UserController() {
    }


    public UserServiceImpl getUserService(){
          return userService;
    }

@Resource(name = "userService")
    public void setUserService(UserServiceImpl userService){
           this.userService=userService;
    }



    public  void addId(int id){
        getUserService().addId(id);
    }
    public void  addUserName(String userName){
        getUserService().addUserName(userName);
    }



}
