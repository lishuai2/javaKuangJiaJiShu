package model;

/**
 * Created by home on 2017/9/21.
 */
public class User {
    public int id;
    public String  username;

    public User() {
    }

    public User(int id, String username) {
        this.id = id;
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "model.User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                '}';
    }
}
