package service;

import dao.UserDaoImpl;

/**
 * Created by home on 2017/9/21.
 */
public class UserServiceImpl implements IUserService {
// 在这个service层里面，申明一个dao层的userDao，这里的userDao是这个class里面的属性，
// 有属性的地方就有set和get方法，这就是依赖注入dao层的东西。

    private UserDaoImpl userDao;
  //  private UserDaoImpl JDBCuserDao;


    public void setUserDao(UserDaoImpl userDao) {
        this.userDao = userDao;
    }

    public UserDaoImpl getUserDao() {
        return userDao;
    }




    @Override
    public void addId(int id) {
       getUserDao().addId(id);
    }

    @Override
    public void addUserName(String userName) {
        getUserDao().addUserName( userName);

    }



}



