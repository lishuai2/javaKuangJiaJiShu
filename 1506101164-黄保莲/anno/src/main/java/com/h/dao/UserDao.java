package com.h.dao;

import com.h.model.User;

/**
 * Created by hbl on 2017/9/21.
 */
public interface UserDao {

         void add(User user);

         User load(int id);


}
