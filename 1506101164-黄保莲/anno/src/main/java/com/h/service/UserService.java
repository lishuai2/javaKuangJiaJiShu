package com.h.service;

import com.h.model.User;

/**
 * Created by hbl on 2017/9/21.
 */
public interface UserService {
    void add(User user);

    void load(int id);
}
