package com.h.controller;

import com.h.model.User;
import com.h.service.UserService;

import java.util.List;

/**
 * Created by hbl on 2017/9/21.
 */

public class UserController {
    private UserService userService;
    private User user;
    private  int id;
    List<String> name;

    public List<String> getName() {
        return name;
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void load(){
        userService.load(id);
    }
}
