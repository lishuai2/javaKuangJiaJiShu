package com.h.test.java;

import com.h.controller.UserController;
import com.h.model.User;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by hbl on 2017/9/21.
 */


public class TestSpring {
    //通过工厂获取Spring的对象
    BeanFactory factory = new ClassPathXmlApplicationContext("beans.xml");



    @Test
    public void testController() {
        UserController uc1 = (UserController) factory.getBean("userController", UserController.class);
        uc1.add();
        User user = new User(1,12, "冷锋");
        UserController uc = (UserController) factory.getBean("userController", UserController.class);
        uc.setUser(user);
        uc.add();
        uc.load();
    }
}
