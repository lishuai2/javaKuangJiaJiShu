package com.h.model;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * Created by hbl on 2017/10/19.
 */
@Component("LogAspect")//让spring 管理LogAspect这个类
@Aspect//声明是个切面
public class LogAspect {
    //第一个*----任意返回值
    //第二个*----表示com.h.dao包中所有的类
    //第三个*----以add开头的所有方法
    //。。-------表示任意参数
    @Before("execution(* com.h.dao.*.add*(..))||execution(* com.h.dao.*.load*(..))")
    public void logStart(){
        Log.log("执行了操作");
    }

    @Around("execution(* com.h.dao.*.add*(..))||execution(* com.h.dao.*.load*(..))")
    public void rr(){
        Log.log("执行中操作");
    }
    @After("execution(* com.h.dao.*.add*(..))||execution(* com.h.dao.*.load*(..))")
    public void logend(){
        Log.log("执行完成了操作");
    }
}
