package com.h.controller;

import com.h.model.User;
import com.h.service.UserService;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * Created by hbl on 2017/9/21.
 */

@Controller

public class UserController {
    private UserService userService;
    private User user;
    private  int id;
    List<String> name;

    public List<String> getName() {
        return name;
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void load() {
        userService.load(id);
    }

    public void add() {

        userService.add(user);
    }

}
