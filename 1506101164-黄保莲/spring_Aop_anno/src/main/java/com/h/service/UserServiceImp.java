package com.h.service;

import com.h.dao.UserDao;
import com.h.model.User;

/**
 * Created by hbl on 2017/9/21.
 */

public class UserServiceImp implements UserService {
    private UserDao userDao;

    public UserDao getUserDao() {
        return userDao;
    }


    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }



    public void add(User user) {
        userDao.add(user);
    }


    public void load(int id) {
        userDao.load(id);
    }
}
