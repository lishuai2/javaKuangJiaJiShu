package com.h.test.java;

import com.h.controller.UserController;
import com.h.model.HelloWord;
import com.h.model.User;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by hbl on 2017/9/21.
 */
public class TestSpring {
    BeanFactory factory = new ClassPathXmlApplicationContext("bean.xml");




    @Test
    public void testHello(){
        HelloWord helloWord = (HelloWord)factory.getBean("HelloWord",HelloWord.class);
        System.out.println(helloWord.hello());
        HelloWord helloWord2 = (HelloWord)factory.getBean("HelloWord",HelloWord.class);
        System.out.println(helloWord2==helloWord);

    }

    @Test
    public void testController(){
        User user = new User(1,12,"你哦好");
        UserController userController = (UserController)factory.getBean("UserController",UserController.class);
        UserController userController1 = (UserController)factory.getBean("UserController",UserController.class);
        userController.setUser(user);
//        userController.add();
//        userController1.add();
    }
}
