package com.h.service;

import com.h.dao.UserDao;
import com.h.model.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by hbl on 2017/9/21.
 */
@Service("UserService")
public class UserServiceImp implements UserService {
    private UserDao userDao;

    public UserDao getUserDao() {
        return userDao;
    }

    @Resource(name = "userDayProxy")
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override

    public void add(User user) {
        userDao.add(user);
    }

    @Override
    public void load(int id) {
        userDao.load(id);
    }
}
