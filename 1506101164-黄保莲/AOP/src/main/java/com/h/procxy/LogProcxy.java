package com.h.procxy;

import com.h.model.Log;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by hbl on 2017/10/12.
 */
public class LogProcxy implements InvocationHandler {

//    创建一个代理对象
    private Object target;

//    创建一个方法，得到代理对象
    public static Object getInstance(Object o){
        LogProcxy proxy = new LogProcxy();
        proxy.target=o;
        Object result = Proxy.newProxyInstance(o.getClass().getClassLoader(),o.getClass().getInterfaces(),proxy);
        return result;
    }

//    有了代理之后，不管这个代理执行什么方法，都会调用invoke方法

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {


            Log.log("只给那些ok");
            Object object = method.invoke(target,args);

        return object;
    }
}
