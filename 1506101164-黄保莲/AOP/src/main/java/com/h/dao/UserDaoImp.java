package com.h.dao;

import com.h.model.Log;
import com.h.model.User;
import org.springframework.stereotype.Repository;

/**
 * Created by hbl on 2017/9/21.
 */
@Repository("UserDao")
public class UserDaoImp implements UserDao {
    @Override
    public void add(User user) {
        Log.log("user add");
        System.out.println("添加user:"+user);
    }

    @Override
    public User load(int id) {
        Log.log("user login");
        System.out.println("登陆"+id);
        return null;
    }
}
