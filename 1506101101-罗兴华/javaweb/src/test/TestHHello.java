import com.spring.lxh.HelloWorld;
import com.spring.lxh.controller.UserController;
import com.spring.lxh.model.User;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by 落叶的思恋 on 2017/9/21.
 */
public class TestHHello {

    BeanFactory factory = new ClassPathXmlApplicationContext("beans.xml");

    @Test
    public void testHello() {
        HelloWorld helloWorld;
        helloWorld = (HelloWorld) factory.getBean("hello", HelloWorld.class);
        helloWorld.hello();
    }

    @Test
    public void testcontroller() {
        User user = new User(1506101101, "罗兴华");
       UserController userController = factory.getBean("userController", UserController.class);

       userController.add(user);

    }
}
