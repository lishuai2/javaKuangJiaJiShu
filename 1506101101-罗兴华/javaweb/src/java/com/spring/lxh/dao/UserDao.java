package com.spring.lxh.dao;

import com.spring.lxh.model.User;

/**
 * Created by 落叶的思恋 on 2017/9/21.
 */
public interface UserDao {
      void add(User user);
      User load(int id);
}
