package com.spring.lxh.dao;

import com.spring.lxh.model.Log;
import com.spring.lxh.model.User;

/**
 * Created by 落叶的思恋 on 2017/10/12.
 */
public class OderDaoImp implements OrderDao {
    @Override
    public void add(User user) {
        Log.log("o add");
        System.out.println("我是orderDao的add User");
    }

    @Override
    public User load(int id) {
        Log.log("o load");
        System.out.println("我是orderDao的load");
        return null;
    }
}
