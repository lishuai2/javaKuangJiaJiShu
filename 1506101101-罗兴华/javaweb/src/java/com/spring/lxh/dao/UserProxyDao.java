package com.spring.lxh.dao;

import com.alibaba.fastjson.JSON;
import com.spring.lxh.model.Log;
import com.spring.lxh.model.User;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Created by 落叶的思恋 on 2017/10/12.
 */
@Repository("UserProxyDao")
public class UserProxyDao implements UserDao {
    public UserDao getUserDao() {
        return userDao;
    }

    @Resource
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    private  UserDao userDao;
    @Override
    public void add(User user) {
        Log.log("add user:"+ JSON.toJSONString(user));
        userDao.add(user);
    }

    @Override
    public User load(int id) {
        Log.log("load user:"+ id);
        userDao.load(id);
        return null;
    }
}
