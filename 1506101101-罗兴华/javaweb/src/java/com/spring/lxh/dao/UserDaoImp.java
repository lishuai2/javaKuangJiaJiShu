package com.spring.lxh.dao;

import com.alibaba.fastjson.JSON;
import com.spring.lxh.model.Log;
import com.spring.lxh.model.User;

import org.springframework.stereotype.Component;


/**
 * Created by 落叶的思恋 on 2017/9/21.
 */
@Component
public class UserDaoImp implements UserDao {
    @Override
    public void add(User user) {
        Log.log("add user"+ JSON.toJSON(user));
        System.out.println("我在添加用户" + JSON.toJSON(user));
    }

    @Override
    public User load(int id) {
        System.out.println("查询到的用户" + id);
        return null;
    }
}
