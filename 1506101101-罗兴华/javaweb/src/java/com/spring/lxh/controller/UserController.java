package com.spring.lxh.controller;

import com.spring.lxh.model.User;
import org.springframework.stereotype.Controller;
import com.spring.lxh.service.UserService;

import javax.annotation.Resource;

/**
 * Created by 落叶的思恋 on 2017/9/21.
 */
@Controller
public class UserController {
    UserService userService;
    User user;


    public UserController(UserService userService) {
        this.userService = userService;
    }


@Resource
    public void setUserService(UserService userService) {

        this.userService = userService;
    }


    public UserService getUserService() {
        return userService;
    }


    public void add(User user) {
        userService.add(user);
    }


    public User load(int id) {
        userService.load(id);
        return null;
    }

    @Resource
    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }


}
