package com.spring.lxh.service;

import com.spring.lxh.model.User;

/**
 * Created by 落叶的思恋 on 2017/9/21.
 */
public interface UserService {
    void add(User user);
    User load(int id);

}
