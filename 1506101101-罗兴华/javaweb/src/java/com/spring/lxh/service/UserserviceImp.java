package com.spring.lxh.service;

import com.spring.lxh.dao.UserDaoImp;
import com.spring.lxh.model.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by 落叶的思恋 on 2017/9/21.
 */
@Service("UserService")
public class UserserviceImp implements UserService {
    UserDaoImp userDaoImp;
    private UserDaoImp userDao;

    @Resource(name = "UserProxyDao")
    public void setUserDao(UserDaoImp UserDao) {
        userDao = UserDao;
    }

    public UserDaoImp getUserDao() {
        return userDao;
    }

    @Override
    public void add(User user) {
        getUserDao().add(user);
    }

    @Override
    public User load(int id) {
        return getUserDao().load(id);
    }


}
