package com.spring.lxh.proxy;

import com.spring.lxh.model.Log;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by 落叶的思恋 on 2017/10/12.
 */
public class LogProxy  implements InvocationHandler {
    private  Object target;

    public  static Object getInstance(Object o){
        LogProxy proxy=new LogProxy();
        proxy.target=o;
        //第一个参数要代理对象的
        Object result= Proxy.newProxyInstance(o.getClass().getClassLoader(),o.getClass().getInterfaces(),proxy);
        return result;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Log.log("开始执行代理操作");
        Object obj=method.invoke(target,args);
        return obj;
    }

}
