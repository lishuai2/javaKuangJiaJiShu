package service;

import model.User;

/**
 * Created by yuweimei on 2017/9/21.
 */
public interface IUserService {

    void add(User user);

    void  load(int id);
}
