package dao;

import model.User;
import proxy.LogInfor;

/**
 * Created by yuweimei on 2017/9/21.
 */
public interface IUserDao {
    @LogInfor(value = "添加了用户")
   public void add(User user);
@LogInfor(value = "查询了用户")
  public  void load(int id);
}
