package dao;

import model.Log;
import model.User;

/**
 * Created by yuweimei on 2017/9/21.
 */
public class UserDao implements IUserDao {
    private UserDao userDao;

    @Override
    public void add(User user){
        Log.log("User add");
        System.out.println("add user"+user);
    }
    @Override
    public void load(int id){
        System.out.println("load user"+id);
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public UserDao getUserDao() {
        return userDao;
    }
}
