package proxy;

/**
 * Created by yuweimei on 2017/10/19.
 */
public @interface LogInfor {

    String value();
}
