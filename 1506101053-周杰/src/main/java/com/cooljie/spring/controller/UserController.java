package com.cooljie.spring.controller;

import com.cooljie.spring.model.User;
import com.cooljie.spring.service.UserService;

/**
 * Created by ACER on 2017/10/23.
 */
public class UserController {
    private UserService userService;
    private User user;
    private int id;

    public void add() {
        userService.add(user);
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
