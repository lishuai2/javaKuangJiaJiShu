package com.cooljie.spring.service;

import com.cooljie.spring.model.User;

/**
 * Created by ACER on 2017/10/23.
 */
public interface UserService {
    void add(User user);
    User load(int id);
}
