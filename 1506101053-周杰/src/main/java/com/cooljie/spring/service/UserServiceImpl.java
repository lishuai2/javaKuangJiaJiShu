package com.cooljie.spring.service;

import com.cooljie.spring.dao.UserDao;
import com.cooljie.spring.model.User;

/**
 * Created by ACER on 2017/10/23.
 */
public class UserServiceImpl implements UserService {
    UserDao userDao;

    @Override
    public void add(User user) {
        userDao.add(user);
    }

    @Override
    public User load(int id) {
        return userDao.load(id);
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
