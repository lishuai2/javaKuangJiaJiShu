package com.cooljie.spring.dao;

import com.cooljie.spring.model.User;

/**
 * Created by ACER on 2017/10/23.
 */
public interface UserDao {
    void add(User user);
    User load(int id);
}
