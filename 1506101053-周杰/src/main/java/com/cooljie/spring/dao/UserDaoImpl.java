package com.cooljie.spring.dao;

import com.cooljie.spring.model.User;

/**
 * Created by ACER on 2017/10/23.
 */
public class UserDaoImpl implements UserDao {
    @Override
    public void add(User user) {
        System.out.println("add user:"+user.toString());
    }

    @Override
    public User load(int id) {
        System.out.println("user id:"+id);
        return null;
    }
}
