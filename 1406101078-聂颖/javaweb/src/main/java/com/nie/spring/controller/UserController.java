package main.java.com.nie.spring.controller;


import main.java.com.nie.spring.service.UserServiceImpl;

/**
 * Created by lenovo on 2017/9/21.
 */
public class UserController {
    UserServiceImpl userService;


    public UserServiceImpl getUserService() {
        return userService;
    }

    public void setUserService(UserServiceImpl userService) {
        this.userService = userService;
    }

    public void addId(int id) {
        getUserService().addId(id);
    }

    public void addUsername(String username) {
        getUserService().addUsername(username);
    }


}
