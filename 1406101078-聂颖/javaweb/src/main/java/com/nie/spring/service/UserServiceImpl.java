package main.java.com.nie.spring.service;


import main.java.com.nie.spring.dao.UserDaoImpl;

/**
 * Created by lenovo on 2017/9/21.
 */
public class UserServiceImpl extends IUserService {
    private UserDaoImpl userDao;


private UserDaoImpl JDBCUserDao ;

    @Override
    public void addId(int id) {
        getUserDao().addId(id);
    }

    @Override
    public void addUsername(String username) {
        getUserDao().addUsername(username);
    }


    public void setUserDao(UserDaoImpl userDao) {
        this.userDao = userDao;
    }

    public UserDaoImpl getUserDao() {
        return userDao;
    }

}
