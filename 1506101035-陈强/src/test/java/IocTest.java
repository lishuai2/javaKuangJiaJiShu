import com.coolchen.spring.controller.UserController;
import com.coolchen.spring.model.User;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class IocTest {
    BeanFactory beanFactory=new ClassPathXmlApplicationContext("beans.xml");
    @Test
    public void testController(){
        User user=new User(1,"哈哈");
        UserController userController= (UserController) beanFactory.getBean("userController");
        userController.setUser(user);
        userController.add();

    }
}
