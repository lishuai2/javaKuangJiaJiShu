package com.coolchen.spring.dao;


import com.coolchen.spring.model.User;

public class UserDaoImpl implements UserDao {
    @Override
    public void add(User user) {
        System.out.println("add user:"+user.toString());
    }

    @Override
    public User load(int id) {
        System.out.println("user id:"+id);
        return null;
    }
}
