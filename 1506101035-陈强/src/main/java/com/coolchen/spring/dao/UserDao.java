package com.coolchen.spring.dao;


import com.coolchen.spring.model.User;

public interface UserDao {
    void add(User user);
    User load(int id);
}
