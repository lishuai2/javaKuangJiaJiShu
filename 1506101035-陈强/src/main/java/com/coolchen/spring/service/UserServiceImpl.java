package com.coolchen.spring.service;


import com.coolchen.spring.dao.UserDao;
import com.coolchen.spring.model.User;

public class UserServiceImpl implements UserService {
    UserDao userDao;

    @Override
    public void add(User user) {
        userDao.add(user);
    }

    @Override
    public User load(int id) {
        return userDao.load(id);
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
