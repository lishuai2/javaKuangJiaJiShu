package com.coolchen.spring.service;


import com.coolchen.spring.model.User;

public interface UserService {
    void add(User user);
    User load(int id);
}
