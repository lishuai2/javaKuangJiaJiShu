package com.coolchen.spring.controller;


import com.coolchen.spring.model.User;
import com.coolchen.spring.service.UserService;

public class UserController {
    private UserService userService;
    private User user;
    private int id;

    public void add() {
        userService.add(user);
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
