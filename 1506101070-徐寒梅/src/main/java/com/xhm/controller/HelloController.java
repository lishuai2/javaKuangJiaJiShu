package com.xhm.controller;

/**
 * Created by coolxu on 2017/10/14.
 */

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;
import java.util.Objects;

@Controller
public class HelloController {
    //RequestMapping表示一个url地址
    @RequestMapping({"/hello","/"})

//    第一种controller向页面传值的方式，使用map
//    public String hello(String username,Map<String,Object> context){
//    public String hello(@RequestParam("username") String username){
        //页面网controller传值,当这里使用了RequestParam之后，如果页面不传值，就会返回400错误，也可以不使用RequestParam，也能传值
    //第二种controller向页面传值的方式，使用model
    public String hello(String username,Model model){
        System.out.println("hello");
     //   context.put("username",username); //第一种controller向页面传值的方式，使用map
    //    model.addAttribute("username",username);//第二种controller向页面传值的方式，使用model
        model.addAttribute(username);//默认使用参数的类型作为传值的对象
        System.out.println(username);
        return "hello";
    }
    @RequestMapping({"/welcome"})
    public String welcome(){
        System.out.println("welcome");
        return "welcome";
    }
}
