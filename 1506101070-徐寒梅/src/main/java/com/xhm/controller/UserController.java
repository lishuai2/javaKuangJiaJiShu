package com.xhm.controller;

import com.xhm.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by coolxu on 2017/10/15.
 */
@Controller
@RequestMapping("/user")
public class UserController {
    private Map<String,User> users=new HashMap<String, User>();

    public UserController() {
//        手动添加用户信息
       users.put("erbai",new User("erbai","1111","二百","erbai.com"));
       users.put("songdongye",new User("songdongye","2222","宋冬野","songdongye.com"));
       users.put("mayou",new User("mayou","3333","马有","mayou.com"));
       users.put("mayday",new User("mayday","4444","五月天","mayday.com"));
    }
    //显示用户列表
//    value=上面的map对象，method表示只接收来自于get的请求
    @RequestMapping(value="/users",method = RequestMethod.GET)
    public String list(Model model){

        model.addAttribute("users", users);
        return "user/list";
    }
    //添加用户,当再其他页面点击添加用户，就跳转到add页面，此时是get请求
    @RequestMapping(value = "/add",method = RequestMethod.GET)
    //第二种方式开启modelDriven
    public String add(@ModelAttribute("user")User user){
        /*第一种方式，开启modelDriven
    public String add(Model model){
        model.addAttribute(new User());
        */
        //服务器端跳转
        return "user/add";
    }
    //当在add页面点击添加用户信息，此时是post请求
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public String add(@Validated User user,BindingResult br){//BindingResult为验证结果类，需要紧跟@Validated注解来写
        if (br.hasErrors()){
            //如果有错误，直接跳转回add页面
            return "user/add";
        }
        users.put(user.getUsername(),user);
        //客户端跳转
        return "redirect:/user/users";
    }
    //显示单个用户信息
    @RequestMapping(value = "/{username}",method = RequestMethod.GET)
    public String show(@PathVariable String username,Model model){
        model.addAttribute(users.get(username));
        return "user/show";
    }
}
