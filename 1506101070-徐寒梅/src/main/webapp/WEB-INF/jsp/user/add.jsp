<%--
  Created by IntelliJ IDEA.
  User: coolxu
  Date: 2017/10/15
  Time: 16:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--使用spring的form标签--%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>添加用户</title>
</head>
<body>
<%--在这表单中不用写action--%>
<sf:form method="post" modelAttribute="user">
username:<sf:input path="username"/><sf:errors path="username"/> <br>
password:<sf:password path="password"/><sf:errors path="password"/><br>
nickname;<sf:input path="nickname"/><br>
email:<sf:input path="email"/><sf:errors path="email"/> <br>
<input type="submit" value="添加用户">
</sf:form>
</body>
</html>
