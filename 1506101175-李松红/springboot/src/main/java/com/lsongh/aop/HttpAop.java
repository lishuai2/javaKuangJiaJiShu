package com.lsongh.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by Administrator on 2017/11/9.
 */
@Aspect
@Component
public class HttpAop {
    private final static Logger logger = LoggerFactory.getLogger(HttpAop.class);

    @Pointcut("execution(public * com.lsongh.controller.StudentController.*(..))")
    public void log() {
    }

    @Before("log()")
    public void doBefore(JoinPoint joinPoint) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        logger.warn("url={}", request.getRequestURL());
        logger.warn("ip={}", request.getRemoteAddr());
        logger.warn("method={}", request.getMethod());
        logger.warn("class_method={}", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        logger.warn("args={}", joinPoint.getArgs());
    }

    @After("log()")
    public void doAfter() {

        logger.info("daAfter");
    }

    @AfterReturning(returning = "object", pointcut = "log()")
    public void doAfterReturning(Object object) {
        logger.warn("obiect={}", object.toString());
    }

}
