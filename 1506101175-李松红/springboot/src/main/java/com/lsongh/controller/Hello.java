package com.lsongh.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2017/10/12.
 */
@RestController
public class Hello {

    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public  String  hello(@RequestParam(value = "id",defaultValue = "0",required = false)Integer id){
        return "hello:"+id;
    }
}
