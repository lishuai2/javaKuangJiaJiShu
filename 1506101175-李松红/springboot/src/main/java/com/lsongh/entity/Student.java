package com.lsongh.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import javax.persistence.GeneratedValue;
import javax.validation.constraints.Min;

@Entity
public class Student {
    @Id
    @GeneratedValue
    private Integer stuentId;

    private String studentName;

    private String studentSex;
    @Min(value = 1, message = "为1以上开头！！！")
    private String studentPhone;

    private String studentAddress;

    public Integer getStuentId() {
        return stuentId;
    }

    public void setStuentId(Integer stuentId) {
        this.stuentId = stuentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName == null ? null : studentName.trim();
    }

    public String getStudentSex() {
        return studentSex;
    }

    public void setStudentSex(String studentSex) {
        this.studentSex = studentSex == null ? null : studentSex.trim();
    }

    public String getStudentPhone() {
        return studentPhone;
    }

    public void setStudentPhone(String studentPhone) {
        this.studentPhone = studentPhone == null ? null : studentPhone.trim();
    }

    public String getStudentAddress() {
        return studentAddress;
    }

    public void setStudentAddress(String studentAddress) {
        this.studentAddress = studentAddress == null ? null : studentAddress.trim();
    }

    public Student() {

    }
}