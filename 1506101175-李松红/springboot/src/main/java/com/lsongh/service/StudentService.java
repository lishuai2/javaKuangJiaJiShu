package com.lsongh.service;

import com.lsongh.entity.Student;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 2017/10/19.
 */
public interface StudentService extends JpaRepository<Student,Integer> {
    @Override
    List<Student> findAll();

    @Override
    List<Student> findAll(Sort sort);

    @Override
    List<Student> findAll(Iterable<Integer> iterable);


    @Override
    void flush();


    @Override
    void deleteInBatch(Iterable<Student> iterable);

    @Override
    void deleteAllInBatch();

    @Override
    Student getOne(Integer integer);


}
