package com.zero.spring.dao;

import com.zero.spring.model.User;

/**
 * Created by Zero on 2017/9/21.
 */
public interface IUserDao {

    public void add(User user);

}
