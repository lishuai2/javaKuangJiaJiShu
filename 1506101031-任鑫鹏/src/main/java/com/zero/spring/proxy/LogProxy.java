package com.zero.spring.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by Zero on 2017/10/12.
 */
public class LogProxy implements InvocationHandler {

    //创建一个代理对象
    private Object target;

    //3. 创建一个方法来生o成对象，这个方法的参数是要代理的对象，getInstance所返回的对象就是代理对象
    public static Object getInstance(Object o){

        //3.1 创建LogProxy对象
        LogProxy proxy=new LogProxy();
        proxy.target=o;

        //3.3

        Object result= Proxy.newProxyInstance(o.getClass().getClassLoader(),o.getClass().getInterfaces(),proxy);
        return result;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

     //   Log.log("执行了操作");
        Object obj=method.invoke(target,args);
        return obj;

    }
}
