import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Zero on 2017/9/21.
 */
public class Test {



    BeanFactory beanFactory=new ClassPathXmlApplicationContext("beans.xml");

    HelloWorld helloWorld=beanFactory.getBean("hw", HelloWorld.class);

    @org.junit.Test
    public void test(){

        System.out.println(helloWorld.helloW());

    }

}
