package service;

import dao.IUserDao;
import model.User;

/**
 * Created by Administrator on 2017/9/21.
 */
public class UserServiceImpl implements IUserService {

    private IUserDao userDao;



    public IUserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(IUserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void add(User user) {
        userDao.add(user);
    }

    @Override
    public void load(int id) {
    userDao.load(id);
    }
}
