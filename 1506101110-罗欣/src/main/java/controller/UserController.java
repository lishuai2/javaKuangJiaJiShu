package controller;

import model.User;
import service.IUserService;

import java.util.List;

/**
 * Created by Administrator on 2017/9/21.
 */
public class UserController {
    private IUserService userService;
    private User user;
    private int id;
    List<String> names;

//构造函数的方法注入
//    public UserController(IUserService userService) {
//        this.userService = userService;
//    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }


    public IUserService getUserService() {
        return userService;
    }

    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void add(){
    userService.add(user);
    }

    public void load(){
        userService.load(id);
    }
}
