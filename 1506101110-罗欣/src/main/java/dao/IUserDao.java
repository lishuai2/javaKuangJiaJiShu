package dao;

import model.User;

/**
 * Created by Administrator on 2017/9/21.
 */
public interface IUserDao {

    public void add(User user);
    public void load(int id);

}
