import controller.UserController;
import javafx.application.Application;
import model.HelloWorld;
import model.User;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Administrator on 2017/9/21.
 */
public class TestSpring {
    BeanFactory factory=new ClassPathXmlApplicationContext("beans.xml");

    @Test
    public void testHello(){
        HelloWorld hello=factory.getBean("helloWorld",HelloWorld.class);
        System.out.println(hello.hello());
    }

@Test
    public void testController(){
    User user=new User(1,"罗欣");
    UserController uc=factory.getBean("userController",UserController.class);
    UserController uc1=factory.getBean("userController",UserController.class);
    uc.setUser(user);
    uc.add();
    uc1.add();

//    这里是对在beans.xml文件添加一个实体进行遍历
//    ClassPathXmlApplicationContext content=new  ClassPathXmlApplicationContext("classpath:beans.xml");
//    User user= (User) content.getBean("user");
//    System.out.println(user.getId()+"-------------"+user.getUsername());

}
}
