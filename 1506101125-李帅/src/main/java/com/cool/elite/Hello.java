package com.cool.elite;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2017/10/18.
 */

@RestController
public class Hello {

    @RequestMapping(value = "/")
    public String hello(){
        return "Hello World";
    }
}
